const wa = require("@open-wa/wa-automate");
const whatsappModel = require("../../models/whatsapp.model");
const cliente = require("../../models/catalogos/clientes.model");
const axios = require("axios");
const { ope, writeFile } = require("fs/promises");
const mime = require("mime-types");
const hash = require("hash.js");
const moment = require("moment");
//realtime
const path = require("path");
const { decryptMedia } = require("@open-wa/wa-automate");
//Whatsapp
let conversacionesActivas = new Map();
let BASE_IMAGE_PATH = path.resolve(__dirname, "..", "..", "imageneswhatsapp");
console.log(__dirname);
let RELATIVE_PATH = path.relative(__dirname, BASE_IMAGE_PATH);
console.log(RELATIVE_PATH);
(async () => {
  const PORT = process.env.PORT || 3025;
  function validarMatricula(matricula) {
    if (matricula.length != 10) {
      return false;
    }

    if (isNaN(matricula.substring(0, 4))) {
      return false;
    }

    if (isNaN(matricula.substring(5, 10))) {
      return false;
    }

    if (matricula.substring(4, 5) != "-") {
      return false;
    }

    return true;
  }

  try {
    var clienteWhatsapp = await wa.create().then((client) => client);

    clienteWhatsapp.onMessage(async (message) => {
      // console.log(message);
      // return;
      let from = message.from;
      console.log(from);
      // if (from != "5218121058094@c.us") {
      //   return true;
      // }

      let whatsappModelResult = await whatsappModel
        .findContact(from)
        .then((res) => res)
        .catch((err) => console.log(err));
      //Existe?
      if (whatsappModelResult.length == 0) {
        //NO
        let id_whatsapp = await whatsappModel
          .addUserWhatsApp(from)
          .then((res) => res)
          .catch((err) => {
            console.log("Hubo un error al guardar el usuario");
          });

        let chat_id = await whatsappModel
          .addChatWhatsApp(id_whatsapp)
          .then((response) => response)
          .catch((err) => console.log(err));

        if (message.mimetype) {
          //*SI ENVIA UN VIDEO ***********************
          if (
            message.mimetype != "image/jpeg" &&
            message.mimetype != "image/png"
          ) {
            await clienteWhatsapp.sendText(
              message.from,
              "Lo sentimos no podemos recibir PDF o videos por este medio, si gustar escribenos a este correo"
            );

            await whatsappModel.addMensajeWhatsApp(
              chat_id,
              1,
              `**********Usuario trato de enviar un archivo**********`
            );
            return;
          }
          //*SI ENVIA UN VIDEO ***********************

          const fileName = hash
            .sha256()
            .update(moment.now().toString())
            .digest("hex");
          const extension = mime.extension(message.mimetype);
          const fullFileName = `${fileName}.${extension}`;
          const fullPath = path.join(
            path.relative(__dirname, BASE_IMAGE_PATH),
            fullFileName
          );
          const mediaData = await wa.decryptMedia(message);
          try {
            await writeFile(fullPath, mediaData);
          } catch (error) {
            console.log(error);
          }

          await whatsappModel.addMensajeWhatsAppImage(
            whatsappModelResult[0].idchat,
            1,
            message.caption,
            fileName,
            extension
          );
        } else {
          let mensajeId = await whatsappModel
            .addMensajeWhatsApp(chat_id, 1, message.content)
            .then((response) => response)
            .catch((err) => console.error(err));
        }

        //Cliente o prospecto
        await clienteWhatsapp.sendText(
          from,
          "Hola, gracias por contactar a Fast English, ¿Estas interesado en aprender inglés?, en caso de que ya seas estudiantes de Fast English solamente escribe tu matricula"
        );

        conversacionesActivas.set(`${from}`, { from, id_whatsapp, chat_id });
      } else {
        //si
        //cliente/prospecto

        if (conversacionesActivas.has(from)) {
          let chatActual = conversacionesActivas.get(from);

          /////////////////////GUARDAR EL EL MENSAJE
          if (message.mimetype) {
            //*SI ENVIA UN VIDEO ***********************
            if (
              message.mimetype != "image/jpeg" &&
              message.mimetype != "image/png"
            ) {
              await clienteWhatsapp.sendText(
                message.from,
                "Lo sentimos no podemos recibir PDF o videos por este medio, si gustar escribenos a este correo"
              );

              await whatsappModel.addMensajeWhatsApp(
                chatActual.chat_id,
                1,
                `**********Usuario trato de enviar un archivo**********`
              );

              //*SI ENVIA UN VIDEO ***********************
              return;
            }

            const fileName = hash
              .sha256()
              .update(moment.now().toString())
              .digest("hex");
            const extension = mime.extension(message.mimetype);
            const fullFileName = `${fileName}.${extension}`;
            const fullPath = path.join(
              path.relative(__dirname, BASE_IMAGE_PATH),
              fullFileName
            );
            const mediaData = await wa.decryptMedia(message);
            try {
              await writeFile(fullPath, mediaData);
            } catch (error) {
              console.log(error);
            }

            await whatsappModel.addMensajeWhatsAppImage(
              chatActual.chat_id,
              1,
              message.caption,
              fileName,
              extension
            );
          } else {
            whatsappModel
              .addMensajeWhatsApp(chatActual.chat_id, 1, message.content)
              .then((response) => response)
              .catch((err) => console.error(err));
          }
          /////////////////////////////////

          if (validarMatricula(message.content)) {
            let clienteMatricula = await cliente
              .getClienteByMatricula(message.content)
              .then((res) => res)
              .catch((err) => console.log(err));

            if (clienteMatricula.length == 1) {
              whatsappModel
                .addProspectToChat(
                  response.data.data.id,
                  chatActual.id_whatsapp
                )
                .then((response) => response)
                .catch((err) => console.error(err));
            }
          } else {
            //prospecto
            await axios
              .post("http://localhost:3004/prospectos.add", {
                nombre: message.sender.pushname,
                telefono: from.substring(0, 13),
                email: "",
                estatus: "Primer contacto",
                id_empleado: null,
                source: "Whatsapp",
              })
              .then(function (response) {
                //
                whatsappModel
                  .addProspectToChat(
                    response.data.data.id,
                    chatActual.id_whatsapp
                  )
                  .then((response) => response)
                  .catch((err) => console.error(err));
                //
              })
              .catch(function (error) {
                console.log(error);
              });

            await clienteWhatsapp.sendText(from, "Permiteme un momento");
            conversacionesActivas.delete(from);
          }
        } else {
          //Redirigir
          if (message.mimetype) {
            //*SI ENVIA UN VIDEO ***********************
            if (
              message.mimetype != "image/jpeg" &&
              message.mimetype != "image/png"
            ) {
              await clienteWhatsapp.sendText(
                message.from,
                "Lo sentimos no podemos recibir PDF o videos por este medio, si gustar escribenos a este correo"
              );

              await whatsappModel.addMensajeWhatsApp(
                whatsappModelResult[0].idchat,
                1,
                `**********Usuario trato de enviar un archivo**********`
              );

              await axios
                .post("http://localhost:3004/wpp.income.message", {
                  inM: 1,
                  mensaje: `**********Usuario trato de enviar un video**********`,
                  whatsapp_user: from,
                })
                .then(function (response) {
                  console.log(response.data);
                })
                .catch(function (error) {
                  console.log(error);
                });

              //*SI ENVIA UN VIDEO ***********************
              return;
            }
            const fileName = hash
              .sha256()
              .update(moment.now().toString())
              .digest("hex");
            const extension = mime.extension(message.mimetype);
            const fullFileName = `${fileName}.${extension}`;
            const fullPath = path.join(
              path.relative(__dirname, BASE_IMAGE_PATH),
              fullFileName
            );
            const mediaData = await wa.decryptMedia(message);
            try {
              await writeFile(fullPath, mediaData);
            } catch (error) {
              console.log(error);
            }

            await whatsappModel.addMensajeWhatsAppImage(
              whatsappModelResult[0].idchat,
              1,
              message.caption,
              fileName,
              extension
            );

            await axios
              .post("http://localhost:3004/wpp.income.message", {
                inM: 1,
                mensaje: message.caption,
                whatsapp_user: from,
                mimetype: message.mimetype,
                url_media: fullFileName,
              })
              .then(function (response) {
                console.log(response.data);
              })
              .catch(function (error) {
                console.log(error);
              });
            console.log({
              inM: 1,
              mensaje: message.caption,
              whatsapp_user: from,
              mimetype: message.mimetype,
              url_media: fullFileName,
            });
          } else {
            await whatsappModel.addMensajeWhatsApp(
              whatsappModelResult[0].idchat,
              1,
              message.content
            );

            await axios
              .post("http://localhost:3004/wpp.income.message", {
                inM: 1,
                mensaje: message.content,
                whatsapp_user: from,
              })
              .then(function (response) {
                console.log(response.data);
              })
              .catch(function (error) {
                console.log(error);
              });
          }
        }
      }
    });
  } catch (e) {}
  const express = require("express");
  const app = express();
  const bodyParser = require("body-parser");

  var cors = require("cors");
  app.use(cors());
  var server = require("http").Server(app);
  // parse requests of content-type: application/json
  app.use(bodyParser.json());
  app.post("/", async (req, res) => {
    if (req.body.file) {
      let filePath = path.join(
        path.relative(__dirname, BASE_IMAGE_PATH),
        `${req.body.fileName}.${req.body.fileExtension}`
      );

      await clienteWhatsapp.sendImage(
        req.body.from,
        filePath,
        "imagen",
        req.body.content
      );

      res.status(200).send({ estatus: "ok" });
    } else {
      await clienteWhatsapp.sendText(req.body.from, req.body.content);
      console.log("mensaje");
      res.status(200).send({ estatus: "ok" });
    }
  });

  server.listen(PORT);
})();
