"use strict";

/**
 * Encapsulo todo el codigo para emitiri y escuchar eventos a través de socket
 *
 */

var ioEvents = function (io, clienteWhatsapp) {
  // Rooms namespace
  io.of("/whats").on("connection", function (socket) {
    console.log("contectado");
  });
};

/**
 * Initialize Socket.io
 *
 */

var init = function (app, clienteWhatsapp) {
  var server = require("http").Server(app);
  var io = require("socket.io")(server);

  // Fuerzo a Socket.io a solamente usar "websockets"; y no Long Polling.
  // io.set("transports", ["websocket"]);

  // Define all Events
  ioEvents(io, clienteWhatsapp);

  // Regreso el servidor para que en el main se sete el puerto
  return server;
};

module.exports = init;
